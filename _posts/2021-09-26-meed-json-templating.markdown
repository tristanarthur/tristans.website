---
layout: post
title:  "Meed JSON Templating"
date:   2021-09-26 22:30:00 +1000
categories: meed templating
---
A basic explanation of my work in progress, personal project, [meed][meed] before explaining the planned UI templating system may be necessary. Meed started out as a basic re-occuring, graphical, stats logger, or in simple terms a popup dialogue that queries the user for certain stats, for example mood. As the project has developed I have used it as an opportunity to extend my knowledge of Python at the cost of project scope and complexity. The new, broadened scope is to provide a highly configurable program for generating static GUIs. This provides the need for static templating component.

I want a way to provide a highly configurable way for the user to generate customized UI templates or for templates to be easily shared between users. I initially thought that it would be worth designing my own file format and parser, however, soon realised it would be much more sensible to choose an existing format with well established parsers such as JSON or YAML. Using a pre-established format will allow me to translate the template file into a Python dictionary using an existing parser. The remaining work will be to translate the dictionary into a `PySimpleGUI` window with child components that use the specified parameters. I expect that there will be some complexity in adding a middleman layer between the meed template and `PySimpleGUI` but for the most part it should be a 1:1 mapping of objects and parameters.

Taking this another step further, I think it may be beneficial to provide some common shortcuts for various use cases. For example, a set of radio buttons; a user should not have to define each radio button individually but instead should be able to specify the amount using a single object.


I am planning for the format to be something similar to the following:
{% highlight json %}
{
  "name": "Mood",
  "theme": "DarkGrey",
  "components": [
	{
	  "type": "label",
	  "text": "Mood Score:",
	},
	{
	  "type": "radio_range",
	  "min": 0,
	  "max": 10,
	  "step": 1,
	},
	{
	  "type": "Submit",
	  "text": "Submit",
	}
  ]
}
{% endhighlight %}

This increase in scope will make meed more useful for general GUI tasks outside of just querying for user input in a re-occuring fashion. Meed could then also be used for things such as a notification engine or integrated into other applications as to handle any static GUI components. In extension, meed should not be limited to reading templates from a file but also reading from stdin and providing output through stdout.

[meed]: https://gitlab.com/tristanarthur/meed

